﻿using System.IO;

namespace Services
{
    public class FileService
    {
        public void AppendText(string path,string text)
        {
            File.AppendAllText(path, text);
        }
    }
}
