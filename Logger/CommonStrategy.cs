﻿using LoggerService.Interfaces;

namespace LoggerService
{
	public class CommonStrategy : ILogOutputStrategy
	{
		public void Log(string message)
		{
			new ConsoleStrategy().Log(message);
			new FileStrategy().Log(message);
		}
	}
}
