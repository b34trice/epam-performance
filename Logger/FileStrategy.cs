﻿using LoggerService.Interfaces;
using Services;

namespace LoggerService
{
	public class FileStrategy : ILogOutputStrategy
	{
		public void Log(string message)
		{
			new FileService().AppendText("C:\\Logs.txt", message);
		}
	}
}
