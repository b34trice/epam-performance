﻿namespace LoggerService.Data
{
	public enum LogLevel
	{
		Error,
		Info,
		Debug
	}
}