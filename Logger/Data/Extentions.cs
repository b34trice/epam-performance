﻿using System;

namespace LoggerService.Data
{
	public static class Extentions
	{
		private const string INFO_LEVEL = "Info";
		private const string DEBUG_LEVEL = "Debug";
		private const string ERROR_LEVEL = "Error";


		public static LogLevel ToLogLevel(this string level)
		{
			switch (level)
			{
				case INFO_LEVEL:
					{
						return LogLevel.Info;
					}
				case DEBUG_LEVEL:
					{
						return LogLevel.Debug;
					}
				case ERROR_LEVEL:
					{
						return LogLevel.Error;
					}
				default:
					throw new ArgumentException("Invalid level");
			}
		}
	}
}