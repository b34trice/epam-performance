﻿using LoggerService.Data;
using LoggerService.Interfaces;
using System;

namespace LoggerService
{
    public static class Logger
    {

        private static bool _isEnable = false;

        public static void EnableLogger()
        {
            _isEnable = true;
        }

        public static void DisableLogger()
        {
            _isEnable = false;
        }

        public static void Log(string message, LogLevel level)
        {
            if (!_isEnable)
            {
                return;
            }

            ILogOutputStrategy outputStrategy = null;
            switch (level)
            {
                case LogLevel.Debug:
                    {
                        outputStrategy = new ConsoleStrategy();
                        break;
                    }
                case LogLevel.Info:
                    {
                        outputStrategy = new FileStrategy();
                        break;
                    }
                case LogLevel.Error:
                    {
                        outputStrategy = new CommonStrategy();
                        break;
                    }
            }
            outputStrategy.Log($"{DateTime.Now} | {level} | {message}");
        }
    }
}