﻿namespace LoggerService.Interfaces
{
	public interface ILogOutputStrategy
	{
		void Log(string message);
	}
}
