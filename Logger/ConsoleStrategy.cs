﻿using LoggerService.Interfaces;
using System;

namespace LoggerService
{
	public class ConsoleStrategy : ILogOutputStrategy
	{
		public void Log(string message)
		{
			Console.WriteLine(message);
		}
	}
}
