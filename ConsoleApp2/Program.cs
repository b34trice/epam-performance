﻿using LoggerService.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Reports
{
	class Program
	{
		private static Dictionary<LogLevel, IEnumerable<string>> _message = new Dictionary<LogLevel, IEnumerable<string>>();

		static void Main(string[] args)
		{
			var messages = File.ReadAllLines("C:\\Logs.txt");
			foreach (var message in messages)
			{
				var level = message.Split('|')[1].Trim().ToLogLevel();
				if (_message.ContainsKey(level))
				{
					var messagesList = _message[level].ToList();
					messagesList.Add(message);
					_message[level] = messagesList;
				}
				else
				{
					_message.Add(level, new List<string>() { message });
				}
			}
			Console.WriteLine($"Info messages count = {_message[LogLevel.Info].Count()}");
			Console.WriteLine($"Debug messages count = {_message[LogLevel.Debug].Count()}");
			Console.WriteLine($"Error messages count = {_message[LogLevel.Error].Count()}");

			var errors = _message[LogLevel.Error];
			foreach (var error in errors)
			{
				Console.WriteLine($"Error message: {error}");
			}
		}
	}
}