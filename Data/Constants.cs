﻿namespace Data
{
	public static class Constants
	{
		public const string AUTH_COUNTER_NAME = "AuthCounterName";
		public const string AUTH_COUNTER_LOGIN_CATEGORY = "AuthCounterLogIn";
		public const string AUTH_COUNTER_LOGOUT_CATEGORY = "AuthCounterLogOut";
		public const string AUTH_COUNTER_REGISTER_CATEGORY = "AuthCounterRegister";
	}
}
