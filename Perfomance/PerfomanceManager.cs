﻿using System.Diagnostics;

namespace Perfomance
{
    public class PerfomanceManager
    {
        public void IncrementCounter(string category, string name)
        {
            CreateIfNotExists(category,name);
            using (var counter = new PerformanceCounter(category, name, false))
            {
                counter.Increment();
            }
        }

        private void CreateIfNotExists(string category, string name)
        {
            if (!PerformanceCounterCategory.Exists(category))
            {

                var counterDataCollection = new CounterCreationDataCollection();

                var averageCount64 = new CounterCreationData
                {
                    CounterType = PerformanceCounterType.AverageCount64,
                    CounterName = name
                };
                counterDataCollection.Add(averageCount64);

                var averageCount64Base = new CounterCreationData
                {
                    CounterType = PerformanceCounterType.AverageBase,
                    CounterName = "AverageCounter64SampleBase"
                };
                counterDataCollection.Add(averageCount64Base);

                PerformanceCounterCategory.Create(category,
                    "Demonstrates usage of the AverageCounter64 performance counter type.",
                    PerformanceCounterCategoryType.SingleInstance, counterDataCollection);
            }
        }
    }
}
